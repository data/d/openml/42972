# OpenML dataset: chronic-kidney-disease

https://www.openml.org/d/42972

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: L.Jerlin Rubini
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/chronic_kidney_disease) - 2015
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)  

**Chronic_Kidney_Disease Data Set**

This dataset can be used to predict the chronic kidney disease and it can be collected from the hospital nearly 2 months of period.

### Attribute information

We use 24 + class = 25 ( 11 numeric ,14 nominal) 
1. Age(numerical) 
age in years 
2. Blood Pressure(numerical) 
bp in mm/Hg 
3. Specific Gravity(nominal) 
4. Albumin(nominal) 
5. Sugar(nominal) 
6. Red Blood Cells(nominal) 
7. Pus Cell (nominal) 
8. Pus Cell clumps(nominal) 
9. Bacteria(nominal) 
10. Blood Glucose Random(numerical) 
11.Blood Urea(numerical) 
12. Serum Creatinine(numerical) 
13. Sodium(numerical) 
14. Potassium(numerical) 
15. Hemoglobin(numerical) 
16.Packed Cell Volume(numerical) 
17. White Blood Cell Count(numerical) 
18. Red Blood Cell Count(numerical) 
19. Hypertension(nominal) 
20. Diabetes Mellitus(nominal) 
21. Coronary Artery Disease(nominal) 
22. Appetite(nominal) 
23. Pedal Edema(nominal) 
24. Anemia(nominal) 
25. Class (nominal)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42972) of an [OpenML dataset](https://www.openml.org/d/42972). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42972/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42972/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42972/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

